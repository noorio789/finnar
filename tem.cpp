#include <bits/stdc++.h>
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

using namespace std;
using namespace cv;

Mat ext;

void detectAndDisplay(Mat);

/** Global variables */
String face_cascade_name = "./haarcascades/haarcascade_frontalface_default.xml";
//String eyes_cascade_name = "./haarcascades/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);



/** @function main */
int main()
{
    VideoCapture capture("v.mp4");
    Mat frame;
    Mat sis = imread("./templates/32.png",1);
    if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };
    cout << sis.size() << endl;
    int len = sis.rows;
    int wid = sis.cols;
    Mat frame2;


    if ( !capture.isOpened() ) { printf("--(!)Error opening video capture\n"); return -1; }

    while (  capture.read(frame) )
    {
        if( frame.empty() )
        {
            printf(" --(!) No captured frame -- Break!");
            break;
        }
        frame.copyTo(frame2);
        Mat hsv ;
        Mat mask;
        cvtColor(frame,hsv,COLOR_BGR2HSV);
        Mat masky,maskw;
        inRange(hsv,Scalar(20,100,100), Scalar(30,255,255),masky);
        inRange(hsv,Scalar(130,3,200), Scalar(131,10,220),maskw);
        bitwise_or(masky,maskw,mask);

        Mat stat,cent;
        Mat Blopframe;

        connectedComponentsWithStats(masky,Blopframe,stat,cent,8);

        int cArea = 0,x0 = frame.rows, xf = 0, yf = 0, y0 = frame.cols;
        vector<Vec4i> rect;
        int wArea;
        int mc = 0;
        bool first = true;
        for(int i = 0; i < stat.rows; i++)
        {
           int x = stat.at<int>(Point(0, i));
           int y = stat.at<int>(Point(1, i));
           int w = stat.at<int>(Point(2, i));
           int h = stat.at<int>(Point(3, i));
           int area = stat.at<int>(Point(4,i));

           if(float(h)/float(w) > 1.5 && w < frame.cols && area > 2000 && area < 15000 && mc < 2){
           if(area > cArea && first)
           {
             first = false;
             mc++;
             wArea = area;
             Vec4i r;
             cArea = area;
             r[0] = x;
             r[1] = y;
             r[2] = w;
             r[3] = h;
             rect.push_back(r);
            //  cout << " Data Cooredinates: " << x0 << ' ' << y0 << ' ' << xf << ' ' << yf << endl;
            //  cv::rectangle(frame,Rect(x0,y0,xf,yf),Scalar(0,0,255));
           }
           else if(abs(wArea - area) < 500 && area > cArea)
           {
               mc++;
               wArea = area;
               Vec4i r;
               cArea = area;
               r[0] = x;
               r[1] = y;
               r[2] = w;
               r[3] = h;
               rect.push_back(r);
              //  cout << " Data Cooredinates: " << x0 << ' ' << y0 << ' ' << xf << ' ' << yf << endl;
              //  cv::rectangle(frame,Rect(x0,y0,xf,yf),Scalar(0,0,255));
             }
           }
         }

         for(int i = 0; i < rect.size(); i++)
          {
            //cout << " Data Cooredinates: " << rect[0] << ' ' << rect[1] << ' ' << rect[2] << ' ' << rect[3] << endl;
            cout << " Area: " << cArea << endl;
            cv::rectangle(frame,Rect(rect[i][0],rect[i][1],rect[i][2],rect[i][3]),Scalar(255,0,0));
            Mat img;
            ext = Mat(sis.rows,sis.cols,sis.type(),double(0));
            detectAndDisplay(sis);
            resize(ext,img,Size(rect[i][2],rect[i][3]),0,0,INTER_NEAREST);
            cout << "IMG SIZE: " << img.size() << endl;
            cout << rect[i] << endl;
            img.copyTo(frame.rowRange(rect[i][1], rect[i][1]+rect[i][3]).colRange(rect[i][0], rect[i][0]+rect[i][2]));
            for (int k = rect[i][1]; k < rect[i][1]+rect[i][3]; k++)
              for(int h = rect[i][0]; h < rect[i][0]+rect[i][2]; h++)
                if(frame.at<Vec3b>(k,h) == Vec3b(0,0,0))
                    frame.at<Vec3b>(k,h) = Vec3b(frame2.at<Vec3b>(k,h));
          }
        //  cv::putText(frame,to_string(rect.size()),Point2f(200,200),FONT_HERSHEY_PLAIN,10,Scalar(0,0,255));

        cout << frame.size() << endl;
        //-- Show what you got
        imshow( "Capture", frame );

        int c = waitKey(50);
        if( (char)c == 27 ) { break; } // escape
    }

    return 0;
}

/** @function detectAndDisplay */
void detectAndDisplay( Mat frame )
{
  std::vector<Rect> faces;
  Mat frame_gray;

  cvtColor( frame, frame_gray, CV_BGR2GRAY );
  equalizeHist( frame_gray, frame_gray );

  //-- Detect faces
  face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );

  for( size_t i = 0; i < faces.size(); i++ )
  {
    Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
    //ellipse( frame, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
    cout << " " << faces[i].y << " " << faces[i].x << " " << faces[i].height << " " << faces[i].width << endl;
    frame.rowRange(faces[i].y, faces[i].y + faces[i].height).colRange(faces[i].x, faces[i].x + faces[i].width).copyTo(ext.rowRange(faces[i].y, faces[i].y + faces[i].height).colRange(faces[i].x, faces[i].x + faces[i].width));


    // for( size_t j = 0; j < eyes.size(); j++ )
    //  {
    //    Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
    //    int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
    //    circle( frame, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
    //  }
  }
  cout << "Ana Talee3 :)" << endl;
 }
